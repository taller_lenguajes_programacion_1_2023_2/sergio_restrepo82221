print ("1. Imprimir Hola, mundo!")

print ("¡Hola Mundo!")
print("\n")

print ("2. Variables y asignación.")

variable = 0
print(variable)
variable += 5
print (variable)

print("\n")

print("3. Tipos de datos básicos: int, float, string.")

numeroEntero = 15
numeroFloat = 15.15
cadenaString = "Taller Uno"

print (f"Ejemplo de numero entero: {numeroEntero}"+
       f"\nEjemplo de numero float: {numeroFloat}"+
       f"\nEjemplo de String: {cadenaString}")

print("\n")

print("4. Operaciones aritméticas básicas.")

numeroUno = 5
numeroDos = 25

print (f"Los numeros a manejar son: {numeroUno} - {numeroDos}" +
       f"\nOperación de suma: {numeroUno + numeroDos}" + 
       f"\nOperación de resta: {numeroUno - numeroDos}" +
       f"\nOperación de multiplicación: {numeroUno * numeroDos}" +
       f"\nOperación de división: {float(numeroUno / numeroDos)}") 

print("\n")

print("5. Conversión entre tipos de datos.")

variableX = 88
print(f"La variable al principio es: {variableX}")
print(f"Varible tipo int: {int(variableX)}")
print(f"Varible tipo float: {float(variableX)}")
print(f"Varible tipo String: {str(variableX)}")
print(f"Varible tipo Caracter: {chr(variableX)}")

print("\n")

print("6. Solicitar entrada del usuario.")

variableUsuario = 5
print (f"El numero es: {variableUsuario}")
variableUsuario = input("Ingresa la nueva asignación: ")
print (f"El numero asignado es: {variableUsuario}")

print("\n")

print("7. Condicional if.")

variableIf = int(input("Ingresa un numero mayor a 0: "))

if variableIf >= 0:
    print("Correcto.")
    
print("\n")
   
print("8. Condicional if-else.")

variableIf = int(input("Ingresa un numero positivo o negativo: "))
if  variableIf >= 0:
    print("Este numero es mayor a 0")
else:
    print("Este numero es menor a 0")
    
print("\n")

print("9. Condicional if-elif-else.")

variableIf = int(input("Ingresa un numero positivo o negativo: "))
if variableIf < 50 and variableIf > 0:
    print("Este numero es positivo")
elif variableIf > -50 and variableIf < 0:
    print("Este numero es negativo")
else:
    print("El numero esta entre -50 y 50, o es igual a 0")

print("\n")

#10. Bucle for.
#11. Bucle while.
#11.Uso de break y continue.
#Andres Felipe Callejas Jaramillo6:16
#13.Listas y sus operaciones básicas.
#14.Tuplas y su inmutabilidad.
#15.Conjuntos y operaciones.
#16.Diccionarios y operaciones clave-valor.
#17.List funciones basicas
#Andres Felipe Callejas Jaramillo6:17
#18. Leer un archivo de texto.
#19. Escribir en un archivo de texto.
#20. Modos de apertura: r, w, a, rb, wb.
#21. Trabajar con archivos JSON.
#Crear un DataFrame.
#22.Leer un archivo CSV.
#23. Filtrar datos en un DataFrame.
#24.Operaciones básicas: sum(), mean(), max(), min().
#25.Uso de iloc y loc.
#Andres Felipe Callejas Jaramillo6:18
#26. Agrupar datos con groupby.
#27. Unir DataFrames con merge y concat.
#28. Manipular series temporales.
#29. Exportar un DataFrame a CSV.
#30.Convertir un DataFrame a JSON.