#Sergio Alejandro Restrepo Gil
#1001 652 322
#Ejercicios con el 2

#Ejercicio 2: Escribe un programa que convierta una variable x de tipo float con valor 5.75 a un tipo int.

sarg_x = 5.75
print (sarg_x)
sarg_xEntero = int(sarg_x)
print(sarg_xEntero)

#Ejercicio 12: Dado un numero n, escribe un programa que determine si una variable n es un numero primo o no
def sarg_esPrimo(sarg_n):
    
    if sarg_n <= 1:
        return False
    
    for sarg_x in range(2, int(sarg_n**0.5) + 1): 
       if sarg_n % sarg_x == 0:
            return False 
        
    return True

sarg_n = int(input("Ingresa un numero entero positivo: "))  
if sarg_n < 0:  
    print("Erro, debes ingresar un numero positivo.")
    
if sarg_esPrimo(sarg_n):
        print(f"{sarg_n} es un numero primo.")
else:
     print(f"{sarg_n} no es un numero primo.")
    
#Ejercicio 22: Escribe un programa que elimine el ultimo numero de una lista.

sarg_lista = [1, 2, 3, 4, 5]
sarg_lista.pop()
print(sarg_lista)

#Ejercicio 32: Dado una lista, escribe un programa que devuelva una lista con los elementos ordenados de menor a mayor.

sarg_lista2 = [8, 4, 3, 10, 5]
sarg_listaOrdenada = sorted(sarg_lista2)
print(sarg_listaOrdenada) 

#Ejercicio 42: Simula un archivo csv informacion.csv, escribe un programa que calcule el promedio de una columna llamada edad.

import csv

sarg_sumarEdades = 0
sarg_numerosRegistrados = 0

with open("D:/Estudio/Universidad/sergio_restrepo82221/1er Entregable/informacion.csv", newline='') as sarg_cvsFile:
    sarg_leerCSV = csv.DictReader(sarg_cvsFile)
    
    for sarg_x in sarg_leerCSV:
            sarg_edad = int(sarg_x['edad'])
            sarg_sumarEdades += sarg_edad
            sarg_numerosRegistrados += 1
    
    if sarg_numerosRegistrados > 0:
        sarg_promedioEdades = sarg_sumarEdades / sarg_numerosRegistrados
        sarg_promedioEdades
        print(f"El promedio de edades es: {sarg_promedioEdades:.2f}")
    else:
        print("No se encontraron registros válidos para calcular el promedio.")
        
#Ejercicio 52: Escriba un programa que lea un archivo csv y remplace el valor especifico en una columna determinada.

#Ejercicio 62: Simula un archivo de excel ventas.xlsl con multiples hojas, escribe un programa que combine todas las hojas y las combine en un unico dataframe

#Ejercicio 72: Simula un archivo de excel, escribe un programa que cuente el numero de filas y columnas del archivo 

import pandas as pd 

# Leer el archivo CSV con Pandas
sarg_df = pd.read_csv("D:/Estudio/Universidad/sergio_restrepo82221/1er Entregable/tabla_excel.xlsx")

# Contar el número de filas y columnas
sarg_numeroFilas = len(sarg_df)
sarg_numeroColumnas = len(sarg_df.columns)

print(f"Número de filas: {sarg_numeroFilas}")
print(f"Número de columnas: {sarg_numeroColumnas}")
